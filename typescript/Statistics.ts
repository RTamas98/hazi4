import {Weight} from "./Weight";
import {StatisticsChart} from "./StatisticsChart";

export class Statistics {

    private mainElement:HTMLElement;
    private statisticsChart: StatisticsChart;

    constructor(){
        this.mainElement = document.getElementsByTagName('main')[0];
        this.createButtons();
        this.statisticsChart = new StatisticsChart();
        this.createCurrentWeight();
        this.createStartWeight();
        this.createProgress();
        this.createGraphiconWeek();
    }
    createButtons(){
        const statistics: HTMLElement = document.createElement('H3');
        statistics.innerText = 'Statistics';
        statistics.className = 'History__class';
        this.mainElement.appendChild(statistics);
        const week: HTMLButtonElement = document.createElement('button');
        week.innerText = 'Week';
        week.className = 'selector';
        week.addEventListener("mouseup", this.createGraphiconWeek.bind(this));
        // week.addEventListener("mouseup",this.createGraphiconWeek().bind(this));
        this.mainElement.appendChild(week);
        const month: HTMLButtonElement = document.createElement('button');
        month.innerText = 'Month';
        month.className = 'selector';
        month.addEventListener("mouseup", this.createGraphiconMonth.bind(this));
        this.mainElement.appendChild(month);
        const year: HTMLButtonElement = document.createElement('button');
        year.innerText = 'Year';
        year.className = 'selector';
        year.addEventListener("mouseup", this.createGraphiconYear.bind(this));
        this.mainElement.appendChild(year);
        const lifeTime: HTMLButtonElement = document.createElement('button');
        lifeTime.innerText = 'Lifetime';
        lifeTime.className = 'selector';
        lifeTime.addEventListener("mouseup", this.createGraphiconLifetime.bind(this));
        this.mainElement.appendChild(lifeTime);
    }

    createGraphiconWeek(){
        let labels: Array<string>=[];
        let data: Array<number>=[];
        for(let i = 0; i <= 7; i++){
            if(Weight.weightInfos[i]==null){
                break;
            }
            labels.push(Weight.weightInfos[i]["date"]);
            data.push(Weight.weightInfos[i]["weight"]);
        }
        this.statisticsChart.updateChart(labels,data);
    }

    createGraphiconMonth(){
        let labels: Array<string>=[];
        let data: Array<number>=[];
        let j;

        for(let i = 0; i <= 30; i++){
            if(Weight.weightInfos[i]==null){
                break;
            }
            if (Weight.weightInfos[i]["date"] == j){
                labels.push(" ");
            }else{
                labels.push(Weight.weightInfos[i]["date"]);
                j = Weight.weightInfos[i]["date"];
            }
            data.push(Weight.weightInfos[i]["weight"]);
        }
        this.statisticsChart.updateChart(labels,data);
    }

    createGraphiconYear(){
        let labels: Array<string>=[];
        let data: Array<number>=[];
        for(let i = 0; i <= 365; i++){
            if(Weight.weightInfos[i]==null){
                break;
            }
            labels.push(Weight.weightInfos[i]["date"]);
            data.push(Weight.weightInfos[i]["weight"]);
        }
        this.statisticsChart.updateChart(labels,data);
    }
    createGraphiconLifetime(){
        let labels: Array<string>=[];
        let data: Array<number>=[];
        for(let i = 0; i < Weight.weightInfos.length-1; i++){
            if(Weight.weightInfos[i]==null){
                break;
            }
            labels.push(Weight.weightInfos[i]["date"]);
            data.push(Weight.weightInfos[i]["weight"]);
        }
        this.statisticsChart.updateChart(labels,data);
    }


    createCurrentWeight(){
    const current__weight: HTMLElement = document.createElement('p');
    current__weight.className = 'small__display__box';
    if(Weight.weightInfos.length > 0){
        current__weight.innerText = Weight.weightInfos[Weight.weightInfos.length-1].weight.toFixed(1) + " kg" + '\n' +"Current Weight";
    }
    else {
       current__weight.innerText = "Current Weight";
    }
    current__weight.id = 'current__weight';
    this.mainElement.appendChild(current__weight);
    }

    createStartWeight(){
        const start__weight: HTMLElement = document.createElement('p');
        start__weight.className = 'small__display__box';
        if(Weight.weightInfos.length > 0){
            start__weight.innerText = Weight.weightInfos[0].weight.toFixed(1) + " kg" + '\n' +"start Weight";
        }else {
            start__weight.innerText = "start Weight"
        }
        start__weight.id = 'start__weight';
        this.mainElement.appendChild(start__weight);
    }


    createProgress(){
        let dif:number;
        let difference: string;

        if(Weight.weightInfos.length > 1){
            dif = Weight.weightInfos[Weight.weightInfos.length-1].weight-Weight.weightInfos[0].weight;
        }else {
            dif = 0;
        }
        if(dif < 0){
            difference =  dif.toFixed(1) + " kg";
        }else if (dif > 0){
            difference = "+ " + dif.toFixed(1) + " kg";
        }else {
           difference = "0 kg";
        }

        const difference__weight: HTMLElement = document.createElement('p');
        difference__weight.innerText = difference+ '\n' +"Progress";
        difference__weight.id = 'difference__weight';
        difference__weight.className = 'big__display__box';
        this.mainElement.appendChild(difference__weight);
    }

}


