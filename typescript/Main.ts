import {Weight} from "./Weight";
import {Statistics} from "./Statistics";
import {CacheService} from "./LocalStorageService";
import {StatisticsChart} from "./StatisticsChart"

const weight: Weight = new Weight();
const statistics: Statistics = new Statistics();
