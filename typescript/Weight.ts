import {IWeightInfo, WeightInfo} from "./WeightInfo";
import {CacheService} from "./LocalStorageService";

export class Weight {

    private mainElement: HTMLElement;
    public static readonly weightInfos: IWeightInfo[] = CacheService.getWrightInfos() || [];
    private weight: HTMLInputElement;
    private datepicker: HTMLInputElement;
    private showData: HTMLParagraphElement;
    private myStorage;

    constructor() {
        this.mainElement = document.getElementsByTagName('main')[0];
        this.createView();
        this.createHistory();
        this.CreateTableFromJSON();
        this.myStorage = window.localStorage;
    }


    createView() {

        this.datepicker = document.createElement('input');
        this.datepicker.type = 'date';
        this.datepicker.id = 'datepicker';
        this.datepicker.className = 'datepicker__class';

        this.mainElement.appendChild(this.datepicker);
        this.weight = document.createElement('input');
        this.weight.type = 'text';
        this.weight.id = 'weight';
        this.weight.className = 'weight__class';
        this.mainElement.appendChild(this.weight);
        const kg:HTMLElement = document.createElement('span');
        kg.innerText = 'kg';
        kg.className = 'kg__class';
        this.mainElement.appendChild(kg);
        const Add: HTMLButtonElement = document.createElement('button');
        Add.innerText = 'Add';
        Add.addEventListener("mouseup", this.getValues.bind(this));
        this.mainElement.appendChild(Add);
    }

    createHistory() {
        const History: HTMLElement = document.createElement('H3');
        History.innerText = 'History';
        History.className = 'History__class';
        this.mainElement.appendChild(History);
        this.showData = document.createElement('p');
        this.showData.id = 'showData';
        this.showData.className = 'showData__class'
        this.mainElement.appendChild(this.showData);

    }


    getValues() {
        let currentWeight;
        currentWeight = this.weight.value;
        this.showData.innerText = currentWeight + " kg";
        let weightInfo = {
            weight: parseFloat(this.weight.value),
            date: new Date(this.datepicker.value).toLocaleDateString("hu-HU")
        }
        let today = new Date().toLocaleDateString("hu-HU");
        let yesterday = new Date().getDay() - 1;

        if (weightInfo.date === today) {
            weightInfo.date = "Today at " + new Date().getHours() + " : " + new Date().getMinutes();
        }
        if ((new Date(this.datepicker.value).getDay()) === yesterday) {
            weightInfo.date = "Yesterday at " + new Date().getHours() + " : " + new Date().getMinutes();
        }

        CacheService.saveWeightInfo(weightInfo);
        Weight.weightInfos.push(weightInfo);
        this.CreateTableFromJSON();
    }

    CreateTableFromJSON() {
        const displayedWeightInfos = CacheService.getWrightInfos()?.slice(-10);

        let col = [];
        for (let i = 0; i < displayedWeightInfos.length; i++) {
            for (let key in displayedWeightInfos[i]) {
                if (col.indexOf(key) === -1) {
                    col.push(key);
                }
            }
        }

        let table = document.createElement("table");
        table.className = "table";


        let tr = table.insertRow(-1);
        for (let i = 0; i < col.length; i++) {
            let th = document.createElement("th");
            th.innerHTML = col[i];
            th.className = 'weight__history';

            tr.appendChild(th);
        }

        for (let i = displayedWeightInfos.length-1; i >= 0; i--) {

            tr = table.insertRow(-1);
            for (let j = 0; j < col.length; j++) {
                let tabCell = tr.insertCell(-1);
                if (col[j] == "weight") {
                    tabCell.innerHTML = displayedWeightInfos[i][col[j]] + " kg";
                } else {
                    tabCell.innerHTML = displayedWeightInfos[i][col[j]] ;
                }
            }
        }

        let divContainer = document.getElementById("showData");
        divContainer.innerHTML = "";
        divContainer.appendChild(table);
    }

    getTodayDate() {
        let today: Date = new Date();
        let dd: string;
        if (today.getDay() < 10) {
            dd = "0" + (today.getDay() + 2);
        } else {
            dd = "" + today.getDay();
        }

        let mm: string;
        if (today.getMonth() < 10) {
            mm = "0" + (today.getMonth() + 1);
        } else {
            mm = "" + today.getMonth()
        }
        let yyyy: number = today.getFullYear();
        let string;

        string = yyyy + '.' + mm + '.' + dd;

        return string;
    }

}