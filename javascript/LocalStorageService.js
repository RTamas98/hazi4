export class CacheService {
    constructor() {
    }
    static saveWeightInfos(weightInfos) {
        localStorage.setItem('WrightInfos', JSON.parse(JSON.stringify(weightInfos)));
    }
    static getWrightInfos() {
        // @ts-ignore
        return JSON.parse(localStorage.getItem('WrightInfos'));
    }
}
