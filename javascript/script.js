let currentWeight;
// @ts-ignore
document.getElementById("datepicker").setAttribute("max", getTodayDate());
const weightInfos = [];
function getValues() {
    var _a;
    // @ts-ignore
    currentWeight = document.getElementById("weight").value;
    // @ts-ignore
    document.getElementById("currentWeight").innerHTML = currentWeight + " kg";
    weightInfos.unshift({
        // @ts-ignore
        weight: document.getElementById("weight").value,
        // @ts-ignore
        date: document.getElementById("datepicker").value
    });
    console.log(weightInfos.length, weightInfos[weightInfos.length - 1]);
    // @ts-ignore
    document.getElementById("changedPeriod").innerHTML = (_a = weightInfos[7]) === null || _a === void 0 ? void 0 : _a.weight;
    if (weightInfos.length > 10) {
        weightInfos.pop();
    }
    weightInfos.forEach(function (value) {
        console.log(value);
    });
    CreateTableFromJSON();
}
function CreateTableFromJSON() {
    //weightInfos.reverse();
    // EXTRACT VALUE FOR HTML HEADER.
    // ('Book ID', 'Book Name', 'Category' and 'Price')
    var col = [];
    for (var i = 0; i < weightInfos.length; i++) {
        for (var key in weightInfos[i]) {
            if (col.indexOf(key) === -1) {
                col.push(key);
            }
        }
    }
    // CREATE DYNAMIC TABLE.
    var table = document.createElement("table");
    table.className = "table";
    // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.
    var tr = table.insertRow(-1); // TABLE ROW.
    for (var i = 0; i < col.length; i++) {
        var th = document.createElement("th"); // TABLE HEADER.
        th.innerHTML = col[i];
        tr.appendChild(th);
    }
    // ADD JSON DATA TO THE TABLE AS ROWS.
    for (var i = 0; i < weightInfos.length; i++) {
        tr = table.insertRow(-1);
        for (var j = 0; j < col.length; j++) {
            var tabCell = tr.insertCell(-1);
            if (col[j] == "weight") {
                tabCell.innerHTML = weightInfos[i][col[j]] + " kg";
            }
            else {
                tabCell.innerHTML = weightInfos[i][col[j]];
            }
            //tabCell.innerHTML = weightInfos[i][col[j]];
        }
    }
    // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
    let divContainer = document.getElementById("showData");
    divContainer.innerHTML = "";
    divContainer.appendChild(table);
}
function getTodayDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 0; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 9) {
        dd = '-1' + dd;
    }
    if (mm < 9) {
        mm = '-1' + mm;
    }
    today = yyyy + '-' + mm + '-' + dd;
    console.log(today);
    return today;
}
export {};
