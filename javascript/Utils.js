export class Utils {
    static getTodayDate() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 0; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 9) {
            dd = '-1' + dd;
        }
        if (mm < 9) {
            mm = '-1' + mm;
        }
        today = yyyy + '-' + mm + '-' + dd;
        console.log(today);
        return today;
    }
}
