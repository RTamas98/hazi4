export class WeightInfo {
    constructor(date, weight) {
        this.date = date;
        this.weight = weight;
        this.validateDate(date);
    }
    validateDate(date) {
        if (date.getTime() > new Date().getTime()) {
            throw new Error("Date is greather than today");
        }
    }
    getInfos() {
        return {
            date: this.date,
            weight: this.weight
        };
    }
}
